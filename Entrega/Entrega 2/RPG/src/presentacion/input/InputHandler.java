package presentacion.input;

import java.util.HashMap;

import org.lwjgl.input.Keyboard;

public class InputHandler {
	/**
	 * Each cycle checks for new input
	 */
	public static HashMap<Integer, Boolean> getInput() {
		HashMap<Integer, Boolean> inputMap = new HashMap<Integer, Boolean>();
		while (Keyboard.next()) {
			inputMap.put(Keyboard.getEventKey(), Keyboard.getEventKeyState());
		}
		return inputMap;
	}

}
