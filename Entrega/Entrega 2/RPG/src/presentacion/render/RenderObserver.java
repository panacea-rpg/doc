package presentacion.render;

import java.util.HashMap;

import org.newdawn.slick.opengl.Texture;

public interface RenderObserver {
	//map es la matriz de fronteras.
	//mapTextures la matriz de Animation que el BL pide al DAO que cargue a partir de AnimationTransfer
	//npcTextures es el mapa de texturas de npc y entities.
	public void onLevelCreated(boolean map[][], Animation mapTextures[],HashMap<String,Texture[][]> npcTextures, String levelName);
	public void onInitialMenuChanged(String ID,int selectedOption);
	public void executeIntroAnimation();
	public void onCrazyActivated();
	public void onCrazyDesactivated();
	public void onAudioChanged(String audio);
	public void onExit();
	
}
