package presentacion.render;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

public class TextRender {
	private TrueTypeFont font2;
	public TextRender(String font, float fontsize) {
		this.initFonts(font, fontsize);
	}

	private void initFonts(String font, float fontsize) {
		
        InputStream is = null;
        /* TODO */
       // load font from file
       try {
         is = org.newdawn.slick.util.ResourceLoader.getResourceAsStream("res/fonts/"+font+".ttf");
            
       } catch (Exception e) {
           e.printStackTrace();
       }

       Font awtFont2 = null;
		try {
			awtFont2 = Font.createFont(Font.TRUETYPE_FONT, is);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       awtFont2 = awtFont2.deriveFont(fontsize); // set font size
        font2 = new TrueTypeFont(awtFont2, true);
	}

	/**
	 * Font drawer. USING DEPRECATED METHODS
	 */
	@SuppressWarnings("deprecation")
	public void drawText(int i,int j,String s) {
		
         font2.drawString(i, j, s);
        
	}
	public void drawText(int i,int j,String s,Color c) {
		
        font2.drawString(i, j, s,c);
       
	}
}
