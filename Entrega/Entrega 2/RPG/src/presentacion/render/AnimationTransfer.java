package presentacion.render;

import org.newdawn.slick.opengl.Texture;

public class AnimationTransfer {
	protected Texture[] frames;
    protected int numFrames;
    private String path;
    
    public AnimationTransfer(String path,int numFrames){
    	frames = new Texture[numFrames];
    	this.numFrames = numFrames;
    	this.path = path;
    }
   /* public AnimationTransfer(String name, int numFrames) {
    	this(name,numFrames,"animations");
    }*/

    public AnimationTransfer(String path) {
    	frames  = new Texture[1];
    	this.numFrames = 1;
    	this.path = path;
    }
    //constructora sin parametros
	public AnimationTransfer() {
	}

	public Texture[] getFrames() {
		return frames;
	}
	public void setFrames(Texture[] frames) {
		this.frames = frames;
	}
	public int getNumFrames() {
		return numFrames;
	}
	public void setNumFrames(int numFrames) {
		this.numFrames = numFrames;
	}
	/*public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}*/
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
    
}
