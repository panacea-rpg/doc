package presentacion.render;


import java.util.HashMap;
import java.util.Map;

import negocio.transfer.ItemTransfer;

public class InventoryRender {
	private Animation inventory = new Animation("menus","inventory","inventory");
	private Animation filter = new Animation("menus","inventory","filtro");
	private Animation cell = new Animation("editorRes/cell");
	private View render;
	private static Map<String,Animation> map = new HashMap<String, Animation>();
	private static final float mult = 1.5f;

	public InventoryRender(View render) {
		this.render = render;
		AnimationTransfer[] anims = { inventory,cell,filter};
		render.loadAnimations(anims);
	}
	
	public void render(ItemTransfer t) {
		String[] items = t.getItems();
		render.originCamera();
		float xPos = render.getWIDTH()/ render.getScaleWidth() - inventory.getImageWidth()*mult;
		float yPos = 0;
		float scaleWidth = render.getScaleWidth()*mult;
		float scaleHeight = render.getHEIGHT()/(render.getFov()/15.0f*inventory.getImageHeight())*mult;
		render.renderTextureWithScaling(filter.getFrame(), 0, 0, scaleWidth, scaleHeight);
		render.renderTextureWithScaling(inventory.getFrame(), xPos, yPos, scaleWidth, scaleHeight);
		renderSelectedOption(scaleWidth,scaleHeight,t.getSelectedOption());
		for(int i = 0; i < items.length;++i) {
			renderOption(scaleWidth, scaleHeight, i, items[i]);
		}
	}
	private void renderOption(float scaleWidth,float scaleHeight,int i,String id) {
		float xPos =  (render.getWIDTH()/ render.getScaleWidth() - cell.getImageWidth()*mult);
		float yPos = cell.getImageWidth() * i*mult;
		if(!map.containsKey(id)) load(id);
		render.renderTextureWithScaling(map.get(id).getFrame(),xPos, yPos, scaleWidth, scaleHeight);
	}

	private void renderSelectedOption(float scaleWidth,float scaleHeight,int i) {
		float xPos =  (render.getWIDTH()/ render.getScaleWidth() - cell.getImageWidth()*mult);
		float yPos = cell.getImageWidth() * i*mult;
		render.renderTextureWithScaling(cell.getFrame(),xPos, yPos, scaleWidth, scaleHeight);
	}
	
	private void load(String id) {
		map.put(id, new Animation("menus","inventory",id));
		AnimationTransfer[] anims = {map.get(id)};
		render.loadAnimations(anims);
	}
}
