package presentacion.render;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.io.IOException;
import java.util.HashMap;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import negocio.transfer.EntityRenderTransfer;
import presentacion.Controller;
import presentacion.sound.Sound;

public class View implements RenderObserver {
	private float HEIGHT;
	private float WIDTH;
	private float scaleHeight;
	private float scaleWidth;
	private DialogRender dialogRender;
	private InitialMenuRender menuRender;
	private Controller controller;
	private LevelRender renderLevel;
	private final int cellSize = 32;
	private boolean isCrazyOn;
	private Color crazyColor = new Color(250, 100,100,250);
	private Sound sound;
	private String lastSound;
	private TextRender textrender;
	private InventoryRender inventoryRender;
	private int fov;
	private final static float tamaLetra = 23f;

	public View() {
		fov = 2;
		sound = null;
		lastSound = null;
		this.isCrazyOn = false;
		initDisplay(true);
		initOpenGl();
		escondeMouse();
		HEIGHT = Display.getHeight();
		WIDTH = Display.getWidth();
		this.renderLevel = null;
		inventoryRender = null;
		dialogRender = null;
		controller = new Controller(this);
		this.menuRender = null;
		textrender = new TextRender("arbutus",tamaLetra);
	}

	private void initDisplay(boolean fullscreen) {
		try {
			if (fullscreen)
				Display.setDisplayModeAndFullscreen(Display.getDesktopDisplayMode());
			else
				Display.setDisplayMode(new DisplayMode(700, 700));
			Display.setVSyncEnabled(false);

			Display.setTitle("RPG");
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.out.println("Display Error.");
			System.exit(1);
		}
	}

	public void initOpenGl() {
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getDisplayMode().getWidth(), Display.getDisplayMode().getHeight(), 0, 1, -1);
	}
	
	public void escondeMouse() {
		try {
			Mouse.create();
			 final Cursor emptyCursor = new Cursor(1, 1, 0, 0, 1, BufferUtils.createIntBuffer(1), null);
		        Mouse.setNativeCursor(emptyCursor);
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void init() {
		controller.run();
	}

	@Override
	public void onLevelCreated(boolean[][] map, Animation[] mapTextures, HashMap<String, Texture[][]> npcTextures,String levelName) {
		controller.removeObserverMenu();
		this.menuRender = null;
		this.renderLevel = new LevelRender(this, mapTextures, npcTextures, map,levelName,fov);
	}

	@Override
	public void onInitialMenuChanged(String ID, int selectedOption) {
		if (this.menuRender == null) {
			this.menuRender = new InitialMenuRender(this);
			this.menuRender.onInitialMenuChanged(ID, selectedOption);
			controller.registerObserverMenu(this.menuRender);
			onAudioChanged("menu");
		}
	}
	public void onGameDestroyed() {
		this.renderLevel = null;
	}

	public void onDialogCreated() {
		if (this.dialogRender == null)
			this.dialogRender = new DialogRender(this);
	}

	public void onDialogDestroyed() {
		this.dialogRender = null;
	}

	public void onInventoryCreated() {
		if(inventoryRender == null) inventoryRender = new InventoryRender(this);
	}

	public void onInventoryDestroyed() {
		inventoryRender = null;
	}

	public void executeIntroAnimation() {
		new IntroAnimation(this).play();
	}
	public void executeFinalAnimation() {
		new FinalAnimation(this).play();
	}

	void centerCameraOn(float x, float y) {
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(x * getScaleWidth() - getWIDTH() / 2.0f, getWIDTH() / 2.0f + x * getScaleWidth(),
				getHEIGHT() / 2.0f + y * getScaleHeight(), y * getScaleHeight() - getHEIGHT() / 2.0f, 1, -1);
	}

	public void originCamera() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getDisplayMode().getWidth(), Display.getDisplayMode().getHeight(), 0, 1, -1);
	}

	/**
	 * Updates the screen
	 */
	public void refresh() {
		if (renderLevel != null)
			renderLevel.renderLevel();
		if(inventoryRender != null)
			inventoryRender.render(controller.getInventoryTransfer());
		if (dialogRender != null)
			dialogRender.render(controller.getDialogMenuTransfer());
		if (menuRender != null)
			menuRender.render();

	}

	public void setScalingToFillScreen(int height, int width) {
		scaleHeight = getHEIGHT() / height;
		scaleWidth = getWIDTH() / width;
	}

	protected void renderMap(Texture texture) {
		renderTexture(texture, 0, 0);
	}

	void renderTexture(Animation texture, float xPos, float yPos) {
		renderTexture(texture.getFrame(), xPos, yPos);
	}

	/**
	 * Renders a texture using GL_QUADS
	 * 
	 * @param texture the texture
	 * @param x       the top left x coordinate
	 * @param y       the top left y coordinate
	 */
	void renderTexture(Texture texture, float xPos, float yPos) {
		xPos *= getScaleWidth();
		yPos *= getScaleHeight();
		texture.bind();
		float Xoffset = texture.getImageWidth() * getScaleWidth();
		float Yoffset = texture.getImageHeight() * getScaleHeight();
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(xPos, yPos);
		glTexCoord2f(texture.getWidth(), 0);
		glVertex2f(xPos + Xoffset, yPos);
		glTexCoord2f(texture.getWidth(), texture.getHeight());
		glVertex2f(xPos + Xoffset, yPos + Yoffset);
		glTexCoord2f(0, texture.getHeight());
		glVertex2f(xPos, yPos + Yoffset);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	void renderTextureWithScaling(Texture texture, float xPos, float yPos, float scaleWidth, float scaleHeight) {
		xPos *= getScaleWidth();
		yPos *= getScaleHeight();
		texture.bind();
		float Xoffset = texture.getImageWidth() * scaleWidth;
		float Yoffset = texture.getImageHeight() * scaleHeight;
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(xPos, yPos);
		glTexCoord2f(texture.getWidth(), 0);
		glVertex2f(xPos + Xoffset, yPos);
		glTexCoord2f(texture.getWidth(), texture.getHeight());
		glVertex2f(xPos + Xoffset, yPos + Yoffset);
		glTexCoord2f(0, texture.getHeight());
		glVertex2f(xPos, yPos + Yoffset);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	/**
	 * Loads a texture from given file
	 * 
	 * @param file the png file
	 * @return the loaded texture
	 * 
	 */
	/* TODO */
	public static Texture loadTexturePNG(String file, boolean flipped) {
		try {
			Texture texture = TextureLoader.getTexture("PNG", View.class.getClassLoader().getResourceAsStream(file),
					GL_LINEAR);
			return texture;
		} catch (IOException e) {
			System.out.println("IOException in Texture: " + file);
			return null;
		}
	}

	/**
	 * Loads a texture from given file
	 * 
	 * @param file the jpg file
	 * @return the loaded texture
	 */

	public static Texture loadTextureJPG(String file, boolean flipped) {
		try {
			Texture texture = TextureLoader.getTexture("JPG", View.class.getClassLoader().getResourceAsStream(file),
					GL_LINEAR);
			return texture;
		} catch (IOException e) {
			System.out.println("IOException in Texture: " + file);
			return null;
		}
	}

	public void destroy() {
		Display.destroy();

	}

	public float getScaleWidth() {
		return scaleWidth;
	}

	public float getHEIGHT() {
		return HEIGHT;
	}

	public float getScaleHeight() {
		return scaleHeight;
	}

	public float getWIDTH() {
		return WIDTH;
	}

	public void setScaleHeight(float f) {
		this.scaleHeight = f;

	}

	public void setScaleWidth(float f) {
		this.scaleWidth = f;

	}

	public EntityRenderTransfer[][] getEntitiesRenderings() {
		return controller.getEntitiesRenderings();
	}

	public EntityRenderTransfer getPlayerRendering() {
		return controller.getPlayerRenderin();
	}

	public LevelRender getRenderLevel() {
		return renderLevel;
	}

	public void updateLevelFrontiers(boolean[][] map, String levelName) {
		controller.updateLevelFrontiers(map, levelName);

	}

	public int getCellSize() {
		return cellSize;
	}

	public void loadAnimations(AnimationTransfer[] anims) {
		controller.loadAnimations(anims);
	}

	@Override
	public void onCrazyActivated() {
		this.isCrazyOn = true;
		textrender = new TextRender("locura",tamaLetra);
		textrender.drawText(0, 0, "", crazyColor);
		if(sound != null) {lastSound = sound.getName();sound.stop();sound = null;}
		sound = new Sound("secta");
		sound.loop();
	}

	@Override
	public void onCrazyDesactivated() {
		this.isCrazyOn = false;
		textrender = new TextRender("arbutus",tamaLetra);
		textrender.drawText(0, 0, "", Color.white);
		sound.stop(); sound = null;
		if(lastSound != null) {
			sound = new Sound(lastSound);
			sound.loop();
		}
	}

	@Override
	public void onAudioChanged(String audio) {
		if(this.sound != null) {
			if(this.sound.getName().equals(audio) || this.sound.getName().equals("secta")) {
				lastSound = audio;
				return;
			}
			this.sound.stop();
		}
		this.sound  = new Sound(audio);
		sound.loop();
	}

	@Override
	public void onExit() {
		controller.onExit();
	}

	public void getEncapuchado(HashMap<String, Texture[][]> npcTextures) {
		controller.getEncapuchado(npcTextures);
	}
	public boolean isCrazyOn() {
		return this.isCrazyOn;
	}

	public Color getCrazyColor() {
		return this.crazyColor;
	}

	public TextRender getTextRender() {
		return textrender;
	}

	public float getTamaLetra() {
		return tamaLetra;
	}

	public void setFOV(int levelFov) {
		this.fov = levelFov;
	}

	public void drawText(int i, int j, String string, Color white) {
		textrender.drawText(0, 0, "", Color.white);
	}
	public float getFov() {
		return this.renderLevel.getFov();
	}


}
