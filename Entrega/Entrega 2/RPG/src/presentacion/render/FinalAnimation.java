package presentacion.render;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;

public class FinalAnimation extends Animation {
	private View render;
	private TextRender textRender;
	private static final int r = 255;
	private static final int g = 255;
	private static final int b = 255;
	private int a;
	public FinalAnimation(View render) {
		super("menus/end");
		this.a = 0;
		this.numFrames = 1;
		textRender = new TextRender("arbutus",1);
		this.render = render;
		AnimationTransfer[] anims = { this};
		render.loadAnimations(anims);
	}

	public void play(){
		render.setScalingToFillScreen(frames[0].getImageHeight(), frames[0].getImageWidth());
		render.originCamera();
		for(int i = 0; i< 80; ++i) {
			changeAlpha(1);
			render.renderMap(frames[0]);
			Display.update();
			try {
				Thread.sleep(140);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		render.drawText(0, 0, "", Color.white);
	}
	private void changeAlpha(int step) {
		this.a += step;
		if(a > 255) a = 255;
		if(a < 0) a = 0;
		Color crazyColor = new Color(r,g,b,this.a);
		textRender.drawText(0, 0, "", crazyColor);
	}
}
