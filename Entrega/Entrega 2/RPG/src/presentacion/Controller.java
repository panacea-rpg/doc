package presentacion;

import java.util.HashMap;

import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.Texture;

import negocio.logic.BusinessLayer;
import negocio.logic.Game;
import negocio.transfer.EntityRenderTransfer;
import negocio.transfer.ItemTransfer;
import presentacion.input.InputHandler;
import presentacion.render.AnimationTransfer;
import presentacion.render.RenderObserver;
import presentacion.render.View;

public class Controller {
	private View render;
	private BusinessLayer bl;
	boolean running;

	public Controller(View render) {
		this.render = render;
		bl = new BusinessLayer(render);
	}

	public void run() {

		running = true;
		while (running) {
			HashMap<Integer, Boolean> inputMap = InputHandler.getInput();
			bl.updateInput(inputMap);

			if (Display.isCloseRequested())
				running = false;
			if (!Display.isActive())
				inputMap = new HashMap<Integer, Boolean>();

			// Logic
			bl.refresh();

			// Render
			render.refresh();

			// window updates
			Display.update();
			Display.sync(Game.FPS);
		}
		render.destroy();
		AL.destroy();

	}
	
	public void removeObserverMenu() {
		bl.setMenuObserver(null);
	}

	public void registerObserverMenu(RenderObserver rs) {
		bl.setMenuObserver(rs);
	}

	public EntityRenderTransfer[][] getEntitiesRenderings() {
		return bl.getEntitiesRenderings();
	}

	public EntityRenderTransfer getPlayerRenderin() {
		return bl.getPlayerRendering();
	}

	public void updateLevelFrontiers(boolean[][] map, String levelName) {
		bl.updateLevelFrontiers(map, levelName);

	}

	public void loadAnimations(AnimationTransfer[] anims) {
		bl.loadAnimations(anims);
	}

	public String getDialogMenuTransfer() {
		return bl.getDialogMenuTransfer();
	}

	public void onExit() {
		running = false;
	}

	public void getEncapuchado(HashMap<String, Texture[][]> npcTextures) {
		bl.getEncapuchado(npcTextures);
	}

	public ItemTransfer getInventoryTransfer() {
		return bl.getInventoryTransfer();
	}
	
}
