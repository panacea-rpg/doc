package presentacion.sound;

import java.io.IOException;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Sound {
	private Audio audio;
	private String name;
	public Sound(String name) {
		this.name = name;
		try {
			audio	= AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/audio/" + name + ".wav"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * The audio is played in independent thread
	 */
	public void play() {
		try {
			new Thread() {
				public void run() {
					//tambien existe playAsMusic
					audio.playAsSoundEffect(1.0f, 1.0f, false);
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		//SoundStore.get().poll(0);
	}
	public void loop() {
		try {
			new Thread() {
				public void run() {
					//tambien existe playAsMusic
					audio.playAsSoundEffect(1.0f, 1.0f, true);
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		//SoundStore.get().poll(0);
	}
	public boolean isPlaying() {
		return audio.isPlaying();
	}
	public void stop() {
		audio.stop();
	}

	public String getName() {
		return name;
	}

}
