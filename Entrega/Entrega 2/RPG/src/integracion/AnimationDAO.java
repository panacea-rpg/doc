package integracion;

import static org.lwjgl.opengl.GL11.GL_LINEAR;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import presentacion.render.AnimationTransfer;
import presentacion.render.View;

public class AnimationDAO implements DAO_ReadOnly<AnimationTransfer> {

	@Override
	public void read(AnimationTransfer t) {
		int numFrames = t.getNumFrames();
		Texture[] frames = new Texture[numFrames];  
		
		
		for(int i = 0; i< numFrames; ++i){
			Texture texture = null;
			try {
				texture = TextureLoader.getTexture("PNG", View.class.getClassLoader().
						getResourceAsStream(t.getPath()+Integer.toString(i+1)+".png"),GL_LINEAR);
			} catch (Exception e) {
				System.out.println("Exception in Texture: " + t.getPath()+(i+1)+".png" );
			}
			
    		frames[i]= texture;
    	}
		t.setFrames(frames);
	}

}
