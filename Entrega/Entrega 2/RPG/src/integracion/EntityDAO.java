package integracion;

import static org.lwjgl.opengl.GL11.GL_LINEAR;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import negocio.transfer.DialogTransfer;
import negocio.transfer.EntityUpdateTransfer;
import negocio.transfer.TextureTransfer;

public class EntityDAO implements DAO_Entity {
	private static final String UDiniPath = "res/entitiesStates";

	private static final String facingValues[] = { "front", "back", "right", "left" };
	private static final String facingValues2[] = { "front", "right", "back", "left" };
	private static final int height = 1;
	private static final int width = 1;
	private static final String path = "spritesheets";

	@Override
	public void read(TextureTransfer t) {
		loadTextures1(t);
	}

	private void loadTextures1(TextureTransfer t) {
		Texture textures[][] = new Texture[4][4];

		try {
			for (int i = 0; i < 4; ++i) {
				for (int j = 0; j < 4; ++j) {
					textures[i][j] = loadTexture(t.getName() + "/" + facingValues[i] + Integer.toString(j + 1) + ".png",
							false);
				}
			}
			t.setTextures(textures);
		} catch (Exception e) {
			System.out.println("Failed loading textures for: " + t.getName()
					+ "(ignore this message if it doesn't have textures)");
			t.setTextures(null);
		}
	}

	private static Texture loadTexture(String file, boolean flipped) throws IOException {
		Texture texture = TextureLoader.getTexture("PNG", EntityDAO.class.getClassLoader().getResourceAsStream(file),
				GL_LINEAR);
		return texture;
	}

	private SpriteSheet loadSprites(String file) {
		InputStream i = EntityDAO.class.getClassLoader().getResourceAsStream(path + "/" + file);
		try {
			SpriteSheet ss = new SpriteSheet(file,
					EntityDAO.class.getClassLoader().getResourceAsStream(path + "/" + file + ".png"), width, height);
			return ss;
		} catch (SlickException e) {
			System.out.println("Exception in SpriteSheet: " + file);
			return null;
		}
	}

	@Override
	public void updateLogicData(EntityUpdateTransfer t) {
		// Para todos los tipos de entities
		try {
			DialogDAO dao = new DialogDAO();
			DialogTransfer dt = new DialogTransfer();
			dt.setId(t.getId());
			dt.setLogicState(t.getState());
			dao.read(dt);
			t.setDialog(dt);
		} catch (Exception e) {
			System.out.println(
					"Failed loading the associated dialog for the entity " + t.getId() + " at state " + t.getState());
		}
		
		JSONObject jo = null;

		try {
			jo = new JSONObject(
					new JSONTokener(new FileReader(UDiniPath + "/" + t.getId() + "/" + t.getState() + ".txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			t.setEvent(jo.getString("event"));
			t.setFacing(jo.getInt("facing"));
		} catch (Exception e) {
			System.out.println("Failed loading the associated common data for the entity " + t.getId() + " at state "
					+ t.getState());
		}

		// solo para dynamic
		if (t.getType().equals("dynamic")) {
			try {
				t.setStop(jo.getBoolean("stop"));
				t.setMaxX(jo.getInt("maxX"));
				t.setMaxY(jo.getInt("maxY"));
				t.setMinX(jo.getInt("minY"));
				t.setMinY(jo.getInt("minY"));
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed loading the associated data for the dynamic entity " + t.getId()
						+ " at state " + t.getState());
			}
		}

	}

}
