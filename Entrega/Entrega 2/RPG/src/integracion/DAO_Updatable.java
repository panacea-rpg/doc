package integracion;

public interface DAO_Updatable<T> extends DAO_ReadOnly<T>{
	public void update(T t);
}
