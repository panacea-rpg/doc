package integracion;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import negocio.transfer.DialogTransfer;

public class DialogDAO implements DAO_ReadOnly<DialogTransfer> {

	@Override
	public void read(DialogTransfer t) {

		JSONObject jo = null;
		try {
			InputStreamReader is  = new InputStreamReader(new FileInputStream("res/dialogs/" + t.getId() + "/" + t.getLogicState() + ".txt"), "UTF-8");
			//FileReader fr = new FileReader("res/dialogs/" + t.getId() + "/" + t.getLogicState() + ".txt");
			jo = new JSONObject(
					new JSONTokener(is));
			//fr.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Cargar la secuencia de mensajes del dialogo
		try {
			JSONArray ja = jo.getJSONArray("sequence");
			List<String> sequence = new ArrayList<String>();

			for (int i = 0; i < ja.length(); ++i) {
				sequence.add(ja.getString(i));
			}
			
			t.setConversation(sequence);
		} catch (JSONException e) {
			System.out.println(
					"Failed loading message sequence for dialog " + t.getId() + " at logicState " + t.getLogicState());
		}

	}

}
