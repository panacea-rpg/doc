package integracion;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import negocio.transfer.DialogTransfer;
import negocio.transfer.TransferItem;

public class ItemDAO implements DAO_ReadOnly<TransferItem> {
    private static final String iniPath = "res/items";

	@Override
	public void read(TransferItem t) {
		if(t.getId() == null) throw new IllegalArgumentException("To load an item you must supply its id");
		
		JSONObject jo = null;

		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/" + t.getId() + ".txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		t.setEvent(jo.getString("event"));
		try {
			DialogDAO dao = new DialogDAO();
			DialogTransfer dt = new DialogTransfer();
			dt.setId(t.getId());//el id del objeto
			dt.setLogicState(0);//state 0 para todo item
			dao.read(dt);
			t.setDialog(dt);
		} catch (Exception e) {
			System.out.println(
					"Failed loading the associated dialog for the item " + t.getId());
		}
	}

}
