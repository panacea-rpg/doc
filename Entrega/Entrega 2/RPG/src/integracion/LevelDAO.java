package integracion;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import negocio.event.TransferCellEvent;
import negocio.transfer.EntityLevelTransfer;
import negocio.transfer.LevelTransfer;
import presentacion.render.AnimationTransfer;

public class LevelDAO implements DAO_Updatable<LevelTransfer> {
	private static final String iniPath = "res/levelData";

	@Override
	public void read(LevelTransfer t) {
		readGeneralData(t);
		readFrontiers(t);// needs already loaded board dimensions and levelName
		readEvents(t);
	}

	/*
	 * Lee de fichero las dimensiones del tablero, las entities con sus posiciones
	 * iniciales y el array de Animations que le corresponden
	 */
	private void readGeneralData(LevelTransfer t) {
		JSONObject jo = null;

		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/general/" + t.getLevelName() + ".txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Leer las dimensiones
		try {
			t.setLevelWidth(jo.getInt("width"));
			t.setLevelHeight(jo.getInt("height"));
		} catch (JSONException e) {
			System.out.println("No se ha conseguido obtener las medidas del tablero");
		}

		// Leer la lista de entities y sus posiciones iniciales
		try {
			JSONArray ja = jo.getJSONArray("entities");
			List<EntityLevelTransfer> entitiesData = new ArrayList<EntityLevelTransfer>();

			for (int i = 0; i < ja.length(); ++i) {
				JSONObject act = ja.getJSONObject(i);
				EntityLevelTransfer aux = new EntityLevelTransfer();

				aux.setName(act.getString("name"));
				aux.setType(act.getString("type"));

				int[] pos = new int[2];
				pos[0] = act.getInt("posX");
				pos[1] = act.getInt("posY");
				aux.setPos(pos);

				entitiesData.add(aux);
			}

			t.setEntitiesData(entitiesData);
		} catch (Exception e) {
			System.out.println("Failed loading" + t.getLevelName() + "'s entities data");
		}

		// Leer el array de AnimationTransfer del nivel(necesito conocer ya Height)
		try {
			AnimationTransfer[] mapTextures = new AnimationTransfer[t.getLevelHeight()];
			JSONArray ja = jo.getJSONArray("animations");

			for (int i = 0; i < ja.length(); ++i) {
				JSONObject texture = ja.getJSONObject(i);

				int ind = texture.getInt("index");
				mapTextures[ind] = new AnimationTransfer();
				mapTextures[ind].setPath(texture.getString("path"));
				mapTextures[ind].setNumFrames(texture.getInt("numFrames"));
			}
			t.setMapTextures(mapTextures);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed loading " + t.getLevelName() + "'s array of animations");
		}

		// Leer la musica del nivel
		try {
          t.setSound(jo.getString("sound"));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed loading " + t.getLevelName() + "'s sound");
		}
	}

	private void readFrontiers(LevelTransfer t) {
		int N = t.getLevelWidth();
		int M = t.getLevelHeight();

		boolean[][] frontiers = new boolean[N][M];

		JSONObject jo = null;
		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/frontiers/" + t.getLevelName() + ".txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			for (int i = 0; i < N; ++i) {
				JSONArray ja = jo.getJSONArray("Frontiers boolean matrix").getJSONArray(i);
				for (int j = 0; j < M; ++j) {
					frontiers[i][j] = ja.getBoolean(j);
				}
			}
		} catch (JSONException e) {
			System.out.println("Failed loading the frontiers board");
		}

		t.setFrontiers(frontiers);
	}

	private void readEvents(LevelTransfer t) {
		JSONObject jo = null;

		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/events/" + t.getLevelName() + ".txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		List<TransferCellEvent> events = new ArrayList<TransferCellEvent>();
		try {
			JSONArray ja = jo.getJSONArray("events");

			for (int i = 0; i < ja.length(); ++i) {
				JSONObject act = ja.getJSONObject(i);
				TransferCellEvent aux = new TransferCellEvent();

				aux.setName(act.getString("name"));
				aux.setX(act.getInt("x"));
				aux.setY(act.getInt("y"));

				if (aux.getName().equals("Transition")) {
					aux.setLevel(act.getString("level"));
					aux.setDestLevel(act.getString("destLevel"));
					aux.setDestX(act.getInt("destX"));
					aux.setDestY(act.getInt("destY"));
				}

				events.add(aux);
			}

			t.setEvents(events);
		} catch (Exception e) {
			System.out.println("Failed loading " + t.getLevelName() + "'s events list.");
		}
	}

	@Override
	public void update(LevelTransfer t) {
		updateFrontiers(t);
	}

	private void updateFrontiers(LevelTransfer t) {
		JSONObject jo = new JSONObject();

		JSONArray jmat = new JSONArray();

		boolean[][] frontiers = t.getFrontiers();
		for (int i = 0; i < frontiers.length; ++i) {
			JSONArray ja = new JSONArray();
			for (int j = 0; j < frontiers[i].length; ++j) {
				ja.put(frontiers[i][j]);
			}
			jmat.put(ja);
		}
		jo.put("Frontiers boolean matrix", jmat);

		try (FileWriter file = new FileWriter(iniPath + "/frontiers/" + t.getLevelName() + ".txt")) {
			file.write(jo.toString());
			System.out
					.println("Fronteras guardadas con exito en " + iniPath + "/frontiers/" + t.getLevelName() + ".txt");
		} catch (IOException e) {
			System.out.println("Error al intentar guardar matriz booleana de fronteras del nivel " + t.getLevelName());
			e.printStackTrace();
		}
	}

}
