package integracion;

import negocio.transfer.EntityUpdateTransfer;
import negocio.transfer.TextureTransfer;

public interface DAO_Entity {
    public void read(TextureTransfer t);
    
    public void updateLogicData(EntityUpdateTransfer t);
}
