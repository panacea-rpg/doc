package negocio.factories;

import negocio.entities.Entity;
import negocio.entities.NPC;
import negocio.entities.StaticEntity;
import negocio.logic.Game;

public class EntityFactory {
    public static Entity createEntity(String id, String type, int x, int y, Game game) {
    	switch(type) {
    	case "static": return new StaticEntity(id, x, y, game);
    	case "dynamic": return new NPC(id,x,y, game);
    	default: return null;
    	}
    }
}
