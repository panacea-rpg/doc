package negocio.logic;

import java.util.List;
import java.util.Map;
import java.util.Random;

import negocio.GameMenus.InventoryMenu;
import negocio.GameMenus.Menu;
import negocio.entities.Entity;
import negocio.entities.Player;
import negocio.event.Event;
import negocio.event.LogicStatesManager;
import negocio.level.Level;
import negocio.transfer.EntityRenderTransfer;
import negocio.transfer.EntityUpdateTransfer;
import negocio.transfer.ItemTransfer;
import negocio.transfer.LevelTransfer;
import negocio.transfer.TransferItem;
import presentacion.render.AnimationTransfer;

public class Game {
	private final String id;
	private final LogicStatesManager LSM;

	private final BusinessLayer BL;
	private final Player player;
	private final Random rand;

	private Level level;
	private Menu menu;
	private final Inventory inventory;
	private boolean crazyMode;

	public static final int LEVELS_NUM = 2;
	public static final int FPS = 100;

	public Game(String id, LogicStatesManager LSM, BusinessLayer BL) {
		this.id = id;
		this.LSM = LSM;
		this.inventory = new Inventory();
		this.rand = new Random();

		this.BL = BL;
		this.crazyMode = false;
		this.player = new Player(this);
	}

	public String getId() {
		return id;
	}
	
	public String getLevelName() {
		return level.getLevelName();
	}
	
	public List<TransferItem> getTransferItems(){
		return inventory.getTransferItems();
	}
	
	public Player getPlayer() {
		return player;
	}

	public Random getRand() {
		return rand;
	}

	public void setInventory(List<TransferItem> list) {
		inventory.setInventory(list, this);
	}

	public void setLevel(Level level, int posX, int posY) {
		this.level = level;
		player.setX(posX);
		player.setY(posY);
		level.setPlayer(player);
	}

	public EntityRenderTransfer getPlayerRendering() {
		return this.player.getRendering();
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	
	public int getEntityState(String id) {
		return LSM.getState(id);
	}

	public void setEntityState(String id, int state) {
		LSM.setState(id, state);
	}
	
	public Map<String, Integer> getEntitiesStates(){
		return LSM.getEntitiesStates();
	}
	

	public EntityRenderTransfer[][] getEntitiesRenderings() { // tambi�n podria bajarse a level
		EntityRenderTransfer[][] mat = new EntityRenderTransfer[level.getLevelWidth()][level.getLevelHeight()];
		for (int i = 0; i < level.getLevelWidth(); ++i) {
			for (int j = 0; j < level.getLevelHeight(); ++j) {
				if (level.getEntityAt(i, j) != null)
					mat[i][j] = level.getEntityAt(i, j).getRendering();
			}
		}

		return mat;
	}
	public boolean isItem(String id) {
		return inventory.isItem(id);
	}

	public boolean isPlayer(int x, int y) {
		return level.getEntityAt(x, y) instanceof Player;
	}

	public boolean isPassable(int x, int y) {
		if (0 <= x && x <= level.getLevelWidth() && 0 <= y && y <= level.getLevelHeight())
			return level.getCell(x, y).isPassable();
		else
			return false;
	}

	public boolean isOccupied(int x, int y) {
		return level.getCell(x, y).isOccupied();
	}

	public void PlayerAction(int x, int y) {
		level.getCell(x, y).PlayerAction();
	}

	public void addToInventory(String idItem) {
		inventory.add(idItem, this);
	}

	public void move(int x0, int y0, int x1, int y1) {
		level.setEntityAt(level.getEntityAt(x0, y0), x1, y1);
		level.setEntityAt(null, x0, y0);
	}

	public void changeLevel(String destLevel, int x, int y) {
		BL.changeLevel(destLevel, x, y);
	}

	public void triggerEvent(int x, int y) {
		Event event = level.getEventAt(x, y);
		if (event != null) {
			event.execute();
		}
	}

	public LevelTransfer getLevelTransfer() {
		return level.createTransfer();
	}

	public int getLevelDimY() {
		return level.getLevelHeight();
	}

	public int getLevelDimX() {
		return level.getLevelWidth();
	}

	public void takeOrders() {
		if (menu != null) {
			menu.takeOrders();
		}else if (LogicInput.INVENTORY.isDown()) {
			setMenu(new InventoryMenu(this, inventory));
			this.onInventoryCreated();
			LogicInput.Menulock();
		} else
			player.takeOrders();
	}

	public void refresh() {
		takeOrders();

		for (int i = 0; i < level.getLevelWidth(); ++i) {
			for (int j = 0; j < level.getLevelHeight(); ++j) {
				level.getCell(i, j).refresh();
			}
		}
	}


	public void updateLevelFrontiers(boolean map[][]) {
		level.setFrontiers(map);
	}

	public AnimationTransfer[] getAnims() {
		return level.getAnims();
	}

	public EntityUpdateTransfer getEntityUpdate(String id, String type, int logicState) {
		return BL.getEntityUpdate(id, type, logicState);
	}

	public Entity getEntity(String id) {
		return level.getEntity(id);
	}

	public String getDialogMenuTransfer() {
		return menu.getDialogMenuTransfer();
	}

	public void onDialogCreated() {
		BL.onDialogCreated();
	}

	public void onDialogDestroyed() {
		BL.onDialogDestroyed();

	}

	public void onInventoryCreated() {
		BL.onInventoryCreated();
	}

	public void onInventoryDestroyed() {
		BL.onInventoryDestroyed();
	}

	public Cell getCell(int x, int y) {
		return level.getCell(x, y);
	}

	public TransferItem readItem(String id) {
		return BL.readItem(id);
	}

	public boolean isCrazyMode() {
		return crazyMode;
	}

	public void setCrazyMode(boolean crazyMode) {
		if(this.crazyMode == crazyMode) return;
		this.crazyMode = crazyMode;
		if (this.crazyMode)
			BL.onCrazyActivated();
		else
			BL.onCrazyDesactivated();
	}

	public ItemTransfer getInventoryTransfer() {
		return menu.getInventoryItems();
	}

	public void endGame() {
		BL.endGame();
	}
	public boolean isNewGame() {
		return LSM.isNewGame();
	}

	public void setEntityCell(int i, int y, Entity object) {
		level.setEntityAt(object, i, y);
	}

}
