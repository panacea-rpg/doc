package negocio.logic;

public enum Facing {
	FRONT(0, LogicInput.DOWN, 0, 1),
	BACK(1,LogicInput.UP, 0, -1),
	RIGHT(2,LogicInput.RIGHT, 1, 0),
	LEFT(3,LogicInput.LEFT, -1, 0);
	
	private final int index;
	private final LogicInput key;
	private final int shiftx;
	private final int shifty;
	
	Facing(int index, LogicInput key, int shiftx, int shifty) {
		this.index = index;
		this.key = key;
		this.shiftx = shiftx;
		this.shifty = shifty;
	}
	public int getInd() {
		return this.index;
	}
	
	public LogicInput getKey() {
		return key;
	}
	
	public int getShiftx() {
		return shiftx;
	}
	
	public int getShifty() {
		return shifty;
	}
	
	public Facing not() {
		if(index % 2 == 0) return Facing.values()[index+1];
		else return Facing.values()[index-1];
	}
	
}
