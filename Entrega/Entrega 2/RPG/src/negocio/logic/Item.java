package negocio.logic;

import negocio.event.Event;

public class Item {
	 private final String id;
     private  Dialog dialog;
     private Event event;
	
	public Item(String id ) {
		this.id = id;
	}
	
	 public String getId() {
		return id;
	}
	
	public Dialog getDialog() {
		return dialog;
	}
	
	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
     
     
}
