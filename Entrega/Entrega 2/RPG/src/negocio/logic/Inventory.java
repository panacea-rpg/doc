package negocio.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import negocio.factories.EventFactory;
import negocio.transfer.TransferItem;

public class Inventory {
     private Map<String, Item> items;
     
     
     public Inventory(){
    	 this.items = new HashMap<String, Item>();
     }
     
     public void setInventory(List<TransferItem> items, Game game) {
    	ListIterator<TransferItem> it = items.listIterator();
    	TransferItem t; Item item;
    	
    	while(it.hasNext()) {
    		t = it.next();
    		item = new Item(t.getId());
    		item.setDialog(new Dialog(t.getDialog()));
    		item.setEvent(EventFactory.createItemEvent(t.getEvent(), item, game));
    		this.items.put(t.getId(), item);
    	}
  
     }
     
     public void add(String idItem, Game game) {
    	 Item item = new Item(idItem);
    	 TransferItem t = game.readItem(idItem);
    	 item.setDialog(new Dialog(t.getDialog()));
    	 item.setEvent(EventFactory.createItemEvent(t.getEvent(), item, game));
    	 items.put(item.getId(), item);
     }
     
     public void remove(String idItem) {
    	 items.remove(idItem);
     }
     
     public List<TransferItem> store() {
    	 List<TransferItem> list = new ArrayList<TransferItem>();
    	 
    	 Iterator<Item> it = items.values().iterator();
    	 while(it.hasNext()) {
    		 Item item = it.next();
    		 TransferItem t = new TransferItem();
    		 t.setId(item.getId());
    		 t.setEvent(item.getEvent().getId());
    		 list.add(t);
    	 }
    	 
    	 return list;
     }

	public Item[] getItems() {
		Item list[] = new Item[items.size()];
		int i = 0;
		for(Map.Entry<String, Item> l: items.entrySet()) {
			list[i] = l.getValue();
			++i;
		}
		
		return list;
		
	}
	
	public List<TransferItem> getTransferItems(){
		List<TransferItem> list = new ArrayList<TransferItem>();
		
		for(Map.Entry<String, Item> l: items.entrySet()) {
			TransferItem t = new TransferItem();
			t.setId(l.getKey());
			list.add(t);
		}
		
		return list;
	}
	public boolean isItem(String id) {
		return items.containsKey(id);
	}
}
