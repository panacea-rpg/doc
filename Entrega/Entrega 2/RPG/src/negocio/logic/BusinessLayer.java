package negocio.logic;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import org.newdawn.slick.opengl.Texture;

import integracion.AnimationDAO;
import integracion.EntityDAO;
import integracion.GameDAO;
import integracion.ItemDAO;
import integracion.LevelDAO;
import negocio.entities.Player;
import negocio.event.LogicStatesManager;
import negocio.event.TransferCellEvent;
import negocio.level.Level;
import negocio.menu.InitialMenu;
import negocio.menu.MainInitialMenu;
import negocio.transfer.EntityLevelTransfer;
import negocio.transfer.EntityRenderTransfer;
import negocio.transfer.EntityUpdateTransfer;
import negocio.transfer.GameTransfer;
import negocio.transfer.ItemTransfer;
import negocio.transfer.LevelTransfer;
import negocio.transfer.TextureTransfer;
import negocio.transfer.TransferItem;
import presentacion.render.Animation;
import presentacion.render.AnimationTransfer;
import presentacion.render.RenderObserver;
import presentacion.render.View;

public class BusinessLayer {
	private Game game;
	private View view;
	private RenderObserver observerMenu;
	
	private InitialMenu menu;
	public BusinessLayer(View render) {
		this.view = render;
		observerMenu = null;
		this.menu = new MainInitialMenu(this);
	}

	private static final String INITIAL = "init";
	
	public void setMenu(InitialMenu menu) {
		this.menu = menu;
	}
	public void exitGame() {
		saveGame(game);
		this.game = null;
		observerMenu = null;
		this.menu = new MainInitialMenu(this);
		view.onGameDestroyed();
		view.onInitialMenuChanged(this.menu.getID(), this.menu.getSelected());
	}
	public void endGame() {
		exitGame();
		view.executeFinalAnimation();
	}

	public void createGame(String id, boolean newGame) {
		GameDAO DAO = new GameDAO();
		GameTransfer t = new GameTransfer();
		
		if (newGame) {
			t.setId(INITIAL);
			DAO.read(t);
			t.setId(id);
		}
		else {
			t.setId(id);
			DAO.read(t);
		}	
		
		game = new Game(id, new LogicStatesManager(t.getEntitiesStates()), this);
		if(game.isNewGame()) view.executeIntroAnimation();
		game.setCrazyMode(t.getCrazy());
		changeLevel(t.getLevelName(), t.getPosX(), t.getPosY());
		game.setInventory(t.getItems());
	}

	public void saveGame(Game game) {
		GameDAO DAO = new GameDAO();
		GameTransfer t = new GameTransfer();
		t.setId(game.getId());
		t.setLevelName(game.getLevelName());
		t.setPosX(game.getPlayer().getX());
		t.setPosY(game.getPlayer().getY());
		t.setEntitiesStates(game.getEntitiesStates());
		t.setItems(game.getTransferItems());
		t.setCrazy(game.isCrazyMode());
		DAO.update(t);
	}

	
	public void changeLevel(String levelName, int playerX, int playerY) {
		LevelDAO levelDAO = new LevelDAO();
		LevelTransfer levelData = new LevelTransfer();
		levelData.setLevelName(levelName);
		levelDAO.read(levelData);

		List<EntityLevelTransfer> entities = levelData.getEntitiesData();
		EntityDAO entityDao = new EntityDAO(); //por favor ponedle un nombre un poco m�s claro. TextureDao o algo asi
		TextureTransfer textureTransfer = new TextureTransfer();
		HashMap<String, Texture[][]> textures = new HashMap<String, Texture[][]>();
		
		ListIterator<EntityLevelTransfer> it = entities.listIterator();
		while (it.hasNext()) {
			String name = it.next().getName();
			textureTransfer.setName(name);
			entityDao.read(textureTransfer);
			textures.put(name,textureTransfer.getTextures());
		}
		
		textureTransfer.setName(Player.PLAYER_NAME);
		entityDao.read(textureTransfer);
		textures.put(Player.PLAYER_NAME, textureTransfer.getTextures());

		Level level = new Level(levelName, levelData.getLevelHeight(), levelData.getLevelWidth(), game);
		Animation[] anims = loadAnimations(levelData.getMapTextures());
		view.onLevelCreated(levelData.getFrontiers(), anims, textures,levelName);
		List<TransferCellEvent> events = levelData.getEvents();
		
		view.onAudioChanged(levelData.getSound());
		level.setEntities(levelData.getEntitiesData(), game); 
		level.setCellEvents(events, game);
		level.setFrontiers(levelData.getFrontiers());
		game.setLevel(level, playerX, playerY);
	}

	// pide al dao que cargue varias animaciones.
	public Animation[] loadAnimations(AnimationTransfer[] animation) {
		AnimationDAO dao = new AnimationDAO();
		Animation[] anims = new Animation[animation.length];
		for (int i = 0; i < animation.length; ++i) {
			if (animation[i] != null) {
				dao.read(animation[i]);
				anims[i] = new Animation(animation[i]);
			}
		}

		return anims;
	}

	public void updateInput(HashMap<Integer, Boolean> inputMap) {
		LogicInput.updateInput(inputMap);
	}
	
	public EntityUpdateTransfer getEntityUpdate(String id, String type, int state) {
		EntityDAO Dao = new EntityDAO();
		EntityUpdateTransfer t = new EntityUpdateTransfer();
		t.setId(id);
		t.setType(type);
		t.setState(state);
		Dao.updateLogicData(t);
		return t;
	}

	public TransferItem readItem(String id) {
		ItemDAO Dao = new ItemDAO();
		TransferItem t = new TransferItem();
		t.setId(id);
		Dao.read(t);
		return t;
	}
	
	public EntityRenderTransfer[][] getEntitiesRenderings() {
		return game.getEntitiesRenderings();
	}

	public EntityRenderTransfer getPlayerRendering() {
		return game.getPlayerRendering();
	}

	public void updateLevelFrontiers(boolean map[][], String levelName) {
		game.updateLevelFrontiers(map);
		LevelDAO ldao = new LevelDAO();
		LevelTransfer levelTransfer = game.getLevelTransfer();
		ldao.update(levelTransfer);
	}

	public void onCrazyActivated() {
		view.onCrazyActivated();
	}
	public void onCrazyDesactivated() {
		view.onCrazyDesactivated();
	}

	public void refresh() {
		if (menu != null) {
			menu.takeOrders();
			if (menu != null) {
				view.onInitialMenuChanged(menu.getID(), menu.getSelected());
				observerMenu.onInitialMenuChanged(menu.getID(), menu.getSelected());
			}
		} else {
			game.refresh();
		}
		if(LogicInput.EXIT.isDown())this.exitGame();
	}

	public String getDialogMenuTransfer() {
		return game.getDialogMenuTransfer();
	}

	public void setMenuObserver(RenderObserver object) {
		this.observerMenu = object;
	}


	public void onDialogCreated() {
		view.onDialogCreated();
	}

	public void onDialogDestroyed() {
		view.onDialogDestroyed();
	}
	public void getEncapuchado(HashMap<String, Texture[][]> npcTextures) {
		EntityDAO dao = new EntityDAO();
		TextureTransfer t = new TextureTransfer();
		t.setName("Sectarian");
		dao.read(t);
		npcTextures.put("Sectarian", t.getTextures());
	}

	public void onExit() {
		view.onExit();
	}

	public void onInventoryCreated() {
		view.onInventoryCreated();
	}

	public void onInventoryDestroyed() {
		view.onInventoryDestroyed();
	}

	public ItemTransfer getInventoryTransfer() {
		return game.getInventoryTransfer();
	}

	public void setFOV(int levelFov) {
		view.setFOV(levelFov);
	}
}
