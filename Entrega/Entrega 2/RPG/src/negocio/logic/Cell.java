package negocio.logic;

import negocio.entities.Entity;
import negocio.event.Event;

public class Cell {
	private Entity entity;
	private Item item;
	private boolean passable;
	private Event event;
	
	public Cell() {
		this.entity = null;
		this.item = null;
		this.passable = true;
		this.event = null;
	}
	public void setPassable(boolean passable) {
		this.passable = passable;
	}
	
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}


	public Item getItem() {
		return item;
	}


	public void setItem(Item item) {
		this.item = item;
	}

    public void setEvent(Event event) {
    	this.event = event;
    }
    
    public Event getEvent() {
    	return event;
    }
	
	public boolean isPassable() {
		return passable;
	}
	
	public boolean isOccupied(){
		return (entity != null);
	}

	public void refresh(){
		if(entity != null) entity.refresh();
	}
	
	public void PlayerAction(){
		if(entity != null){
			entity.interact();
		}
	}
}
