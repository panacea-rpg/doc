package negocio.transfer;

import org.newdawn.slick.Image;
import org.newdawn.slick.opengl.Texture;

public class TextureTransfer {
	private String name;
	private Texture[][] textures;
	private Image[][] images;
	
	public String getName() {
		return name;
	}
	public Texture[][] getTextures() {
		return textures;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setTextures(Texture[][] textures) {
		this.textures = textures;
	}
	public void setImages(Image[][] images) {
		this.images = images;
	}
	public Image[][] getImages(){
		return this.images;
	}

}
