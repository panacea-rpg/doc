package negocio.transfer;

public class TransferItem {
	private String id;
    private DialogTransfer dialog;
    private String event;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public DialogTransfer getDialog() {
		return dialog;
	}
	public void setDialog(DialogTransfer dialog) {
		this.dialog = dialog;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
    
    
}
