package negocio.transfer;

import java.util.List;
import java.util.Map;

public class GameTransfer {
	 private String id;
     private String levelName;
     private int posX;
     private int posY;
     private Map<String, Integer> entitiesStates;
     private List<TransferItem> items;
     private boolean crazy;
     
     public void setId(String id) {
 		this.id = id;		
 	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	public Map<String, Integer> getEntitiesStates() {
		return entitiesStates;
	}
	public void setEntitiesStates(Map<String, Integer> entitiesStates) {
		this.entitiesStates = entitiesStates;
	}
	public String getId() {
		return id;
	}
	public List<TransferItem> getItems() {
		return items;
	}
	public void setItems(List<TransferItem> items) {
		this.items = items;
	}

	public void setCrazy(boolean boolean1) {
		this.crazy = boolean1;
	}
	public boolean getCrazy() {
		return crazy;
	}
	
     
     
}
