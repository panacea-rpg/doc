package negocio.transfer;

import java.util.List;

public class DialogTransfer {
	private String id;
	private int logicState;
	private List<String> conversation;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getLogicState() {
		return logicState;
	}
	public void setLogicState(int logicState) {
		this.logicState = logicState;
	}
	public List<String> getConversation() {
		return conversation;
	}
	public void setConversation(List<String> conversation) {
		this.conversation = conversation;
	}
	
}
