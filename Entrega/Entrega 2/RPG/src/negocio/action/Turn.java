package negocio.action;

import negocio.entities.DynamicEntity;
import negocio.logic.Facing;

public class Turn extends Action{
    private Facing facing;
    
    public Turn(DynamicEntity entity, Facing facing) {
    	super(entity);
    	this.facing = facing;
    }
    
    public void execute() {
    	entity.setFacing(facing);
    }
}
