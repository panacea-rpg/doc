package negocio.action;

import negocio.entities.DynamicEntity;
import negocio.logic.Facing;

public class Advance extends Action {
     private Facing facing;

     public Advance(DynamicEntity entity, Facing facing){
    	 super(entity);
    	 this.facing = facing;
     }
	
	public void execute() {
		entity.setFacing(facing);
		entity.advance();
	}
}
