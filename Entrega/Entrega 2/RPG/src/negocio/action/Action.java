package negocio.action;

import negocio.entities.DynamicEntity;

public abstract class Action {
	protected DynamicEntity entity;
	
	public Action(DynamicEntity entity) {
		this.entity= entity;
	}
	
    public abstract void execute();
} 
