package negocio.level;


import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import negocio.entities.Entity;
import negocio.entities.Player;
import negocio.event.Event;
import negocio.event.TransferCellEvent;
import negocio.factories.EntityFactory;
import negocio.factories.EventFactory;
import negocio.logic.Cell;
import negocio.logic.Game;
import negocio.transfer.EntityLevelTransfer;
import negocio.transfer.LevelTransfer;
import presentacion.render.AnimationTransfer;;

public class Level {
	protected final String levelName;
	protected Cell[][] board;
	protected final int levelWidth;
	protected final int levelHeight;
	protected  final Map<String, Entity> entities; 
	
	protected AnimationTransfer[] mapTextures;
	private int ciclos;
	
	
	public Level(String levelName, int levelHeight,int levelWidth,  Game game) {
		this.levelName = levelName;
		this.levelHeight = levelHeight;
		this.levelWidth = levelWidth;
		this.board = new Cell[levelWidth][levelHeight];
		this.entities = new HashMap<String, Entity>();
		
		for(int i = 0; i < levelWidth;++i) {
			for(int j = 0; j < levelHeight; ++j) {
				board[i][j] = new Cell();
			}
		}	
		
		//Esto es de render
		this.ciclos = 0;
 
		
	}
	
	public String getLevelName() {
		return levelName;
	}
	
	public Cell getCell(int x, int y) {
		return board[x][y];
	}
	
	public Entity getEntity(String id) {
		return entities.get(id);
	}
	
	public Entity getEntityAt(int x, int y) {
		return board[x][y].getEntity();
	}
	
	public void setEntityAt(Entity entity, int x, int y) {
		board[x][y].setEntity(entity);
	}
	
	public Event getEventAt(int x, int y) {
		return board[x][y].getEvent();
	}
	
	public void setEventAt(Event event, int x, int y) {
		board[x][y].setEvent(event);
	}
	
	public void setBoard(Cell[][] board) {
		this.board = board;
	}
    
	public void setPlayer(Player player) {
		board[player.getX()][ player.getY()].setEntity(player);
	}
	
	public void setEntities(List<EntityLevelTransfer> entities, Game game) {
		ListIterator<EntityLevelTransfer> it = entities.listIterator(); 
		EntityLevelTransfer t; Entity entity; 
		
		while(it.hasNext()) {
			t = it.next();
			if(game.getEntityState(t.getName()) != -1) {
			entity = EntityFactory.createEntity(t.getName(), t.getType(), t.getPos()[0], t.getPos()[1], game);
			this.entities.put(t.getName(), entity);
			board[t.getPos()[0]][t.getPos()[1]].setEntity(entity);
			}
		}
		
	}
     
	public void setCellEvents(List<TransferCellEvent> events, Game game) {
		ListIterator<TransferCellEvent> it = events.listIterator();
		TransferCellEvent t; Event event; int x, y; 
		
		while(it.hasNext()) {
			t = it.next();
			x = t.getX();
			y = t.getY();
			event = EventFactory.createCellEvent(t, game);
			board[x][y].setEvent(event);
		}
	}
	
	public int getLevelWidth(){
		return levelWidth;
	}
	
	public int getLevelHeight(){
		return levelHeight;
	}
	
	public boolean[][] getFrontiers(){
		try {
			boolean[][] frontiers = new boolean[levelWidth][levelHeight];
			for(int i = 0; i < levelWidth; ++i) {
				for(int j = 0; j < levelHeight; ++j) {
					frontiers[i][j] = board[i][j].isPassable();
				}
			}
			
			return frontiers;			
		}
		catch(Exception e) {
			System.out.println("You are trying to get the frontiers without them being initializaed correctly"
			+System.getProperty("line.separator"));
			
			return null;
		}
	}
	
	public void setFrontiers(boolean[][] frontiers) throws IllegalArgumentException {
		if(frontiers.length != levelWidth) throw new IllegalArgumentException("Frontiers boolean matrix doesnt have a correct size");
		
		for(int i = 0; i < levelWidth;++i) {
			if(frontiers[i].length != levelHeight) throw new IllegalArgumentException("Frontiers boolean matrix doesnt have a correct size");
			for(int j = 0; j < levelHeight; ++j) {
				board[i][j].setPassable(frontiers[i][j]);
			}
		}
	}
	
	public LevelTransfer createTransfer() {
		LevelTransfer t = new LevelTransfer();
		
		t.setFrontiers(getFrontiers());
		t.setLevelName(levelName);
		t.setLevelWidth(levelWidth);
		t.setLevelHeight(levelHeight);
		t.setMapTextures(mapTextures);
		t.setCiclos(ciclos);
		
		return t;
	}
	


	public AnimationTransfer[] getAnims() {
		return this.mapTextures;
	}
	
	
	
}
