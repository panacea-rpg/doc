package negocio.menuCommand;

import negocio.logic.BusinessLayer;
import negocio.menu.ConfigMenu;
import negocio.menu.HelpInitialMenu;
import negocio.menu.MainInitialMenu;
import negocio.menu.PlayMenu;

public class ChangeMenuCommand extends Command {
	private String destMenu;

	public ChangeMenuCommand(BusinessLayer bl,String destMenu) {
		super(bl);
		this.destMenu = destMenu;
	}
	
	@Override
	public void execute() {
		if(destMenu.equals("MainInitialMenu"))
			this.bl.setMenu(new MainInitialMenu(bl));
		else  if(destMenu.contentEquals("config"))
			this.bl.setMenu(new ConfigMenu(bl));
		else if(destMenu.contentEquals("play"))
			this.bl.setMenu(new PlayMenu(bl));
		else this.bl.setMenu(new HelpInitialMenu(bl,destMenu));
		
	}

}
