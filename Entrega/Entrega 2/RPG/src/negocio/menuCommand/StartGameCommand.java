package negocio.menuCommand;

import negocio.logic.BusinessLayer;

public class StartGameCommand extends Command {
	private boolean newGame;
	public StartGameCommand(BusinessLayer bl,boolean newGame) {
		super(bl);
		this.newGame = newGame;
	}
	@Override
	public void execute() {
		bl.createGame("load", newGame);
		bl.setMenu(null);
	}

}
