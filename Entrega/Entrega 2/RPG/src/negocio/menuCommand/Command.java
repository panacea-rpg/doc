package negocio.menuCommand;

import negocio.logic.BusinessLayer;

public abstract class Command {
	protected BusinessLayer bl;
	public Command( BusinessLayer bl) {
		this.bl = bl;
	}
	
	public abstract void execute();
}
