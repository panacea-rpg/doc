package negocio.entities;

import java.util.LinkedList;
import java.util.Queue;

import negocio.action.Action;
import negocio.event.Event;
import negocio.logic.Dialog;
import negocio.logic.Facing;
import negocio.logic.Game;
import negocio.transfer.EntityRenderTransfer;


public abstract class Entity {
	protected final String id;
	//board position
	protected int x;
	protected int y;
	protected float offset;
	
	//screen position 
	
	protected Facing facing;
	protected Facing prevFacing;
	
	protected Dialog dialog;
	protected int logicState;
	protected Game game;
	protected Event event;
	protected Queue<Action> orders;
	protected boolean act;
	protected boolean talking;
 	//el punto de la animacion en la que se encuentra
	protected int animState; 

	
	
	public Entity(String id, int x,int y,Game game) {
		this.id = id;
		this.game = game;
		this.x = x;
		this.y = y;
		this.offset = 0;
		this.facing = Facing.FRONT;
		this.prevFacing = null;
		this.act = false;
		this.talking = false;
		this.animState = 0;
		this.orders = new LinkedList<Action>();
		this.logicState = -1;
		update();
	}
	
	public String getId() {
		return id;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public Facing getPrevFacing() {
		return prevFacing;
	}
	
	public void setFacing(Facing facing) {
		this.facing = facing;
	}
	public void setAct(boolean act) {
		this.act= act;
	}
	
    public void setTalking(boolean talking) {
	    	 this.talking = talking;
	  }
	
	public EntityRenderTransfer getRendering() {
		return new EntityRenderTransfer(x, y, offset, facing, id, animState);
	}
	
	
	
	protected abstract void update();
	public abstract void interact();
	public abstract void refresh();
	public abstract void addOrder(Action order);
	
	
	
}
