package negocio.entities;

import negocio.GameMenus.DialogMenu;
import negocio.action.Action;
import negocio.factories.EventFactory;
import negocio.logic.Dialog;
import negocio.logic.Facing;
import negocio.logic.Game;
import negocio.transfer.EntityUpdateTransfer;


public class StaticEntity extends Entity{
	public StaticEntity(String id, int x, int y, Game game) {
		super(id, x, y, game);
	}

	public void interact() {
		if (dialog != null) {
			game.setMenu(new DialogMenu(this, dialog, game));
			talking = true;
			game.onDialogCreated();
		}
	}


	public void refresh() {
		update();
	}

	@Override
	protected void update() {
		if(logicState != game.getEntityState(id)) {
			logicState = game.getEntityState(id);
			EntityUpdateTransfer t = game.getEntityUpdate(id, "static", logicState); 
		    dialog = new Dialog(t.getDialog());
		    event = EventFactory.createEntityEvent(t.getEvent(), this, game);
		    facing = Facing.values()[t.getFacing()];
		}
	}
	
	public void addOrder(Action order) {
	}

}
