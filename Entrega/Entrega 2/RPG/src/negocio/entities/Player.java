package negocio.entities;

import negocio.logic.Facing;
import negocio.logic.Game;
import negocio.logic.LogicInput;

public final class Player extends DynamicEntity {
	private Facing ignoredFacing;

	public static final String PLAYER_NAME= "Player";
	
	public Player(Game game) {
		super(PLAYER_NAME ,0, 0, game);
	}
    

	public void takeOrders() {
		if(orders.isEmpty()) {
		if(moving && LogicInput.RUN.isDown())refreshAnimation();
		else if(!moving){
			if (facing.getKey().isDown()) {
				boolean found = false;
	 			for(int i = 0; !found && i < Facing.values().length; ++i) {
					if(Facing.values()[i] != ignoredFacing && Facing.values()[i] != facing
							&& Facing.values()[i].getKey().isDown()) { 
						found = true;
						ignoredFacing = facing;
						facing = Facing.values()[i];
						LogicInput.lock();
					}
				}
				
				advance();
			}
			
			else if (LogicInput.ACTION.isDown()) {
				act = true;
				LogicInput.Menulock();
			} 
			
			else {
				ignoredFacing = null;
				boolean found = false;
				for (int i = 0; !found && i < Facing.values().length; ++i) {
					if (Facing.values()[i].getKey().isDown()) {
						found = true;
						facing = Facing.values()[i];
						LogicInput.lock();
					}
				}
			}
		}
		}
		
	else{
			orders.poll().execute();
	}
	}



	public void refresh() {
		if (act) {
			game.PlayerAction(x + facing.getShiftx(), y + facing.getShifty());
			act = false;
		}
		refreshAnimation();
	}

	public void loadDialog() {}
	public void interact() {}
	protected void update() {
	}
}
