package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.menuCommand.ChangeMenuCommand;

public class HelpInitialMenu extends InitialMenu {

	public HelpInitialMenu(BusinessLayer bl,String ID) {
		super(1, ID, bl);
	}

	@Override
	public void action() {
		new ChangeMenuCommand(bl, "MainInitialMenu").execute();
	}

	@Override
	public void exit() {
		new ChangeMenuCommand(bl, "MainInitialMenu").execute();
	}

}
