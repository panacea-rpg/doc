package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.menuCommand.ChangeFovCommand;
import negocio.menuCommand.ChangeMenuCommand;

public class ConfigMenu extends InitialMenu{

	public ConfigMenu(BusinessLayer bl) {
		super(4, "config", bl);
		this.selectedConfig = 2;
	}

	@Override
	public void action() {
		new ChangeFovCommand(bl,this.selectedConfig).execute();
		new ChangeMenuCommand(bl, "MainInitialMenu").execute();
	}

	@Override
	public void exit() {
		new ChangeMenuCommand(bl, "MainInitialMenu").execute();
	}
	public int getSelected() {
		return this.selectedConfig;
	}

}
