package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.menuCommand.ChangeMenuCommand;
public class MainInitialMenu extends InitialMenu {

	public MainInitialMenu(BusinessLayer bl) {
		super(4, "main", bl);
	}

	@Override
	public void action() {
		switch(this.selectedOption) {
		case 0:
			new ChangeMenuCommand(bl, "play").execute();
			break;
		case 1:
			new ChangeMenuCommand(bl, "config").execute();
			break;
		case 2:
			new ChangeMenuCommand(bl, "ayuda").execute();
			break;
		case 3:
			new ChangeMenuCommand(bl, "creditos").execute();
			break;
		case 4:
			break;
		}
		
	}

	@Override
	public void exit() {
		bl.onExit();
	}

}
