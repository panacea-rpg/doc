package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.menuCommand.ChangeMenuCommand;
import negocio.menuCommand.StartGameCommand;

public class PlayMenu extends InitialMenu {

	public PlayMenu(BusinessLayer bl) {
		super(2, "play", bl);
	}

	@Override
	public void action() {
		new StartGameCommand(bl, this.selectedOption != 0).execute();
	}

	@Override
	public void exit() {
		new ChangeMenuCommand(bl, "MainInitialMenu").execute();
	}

}
