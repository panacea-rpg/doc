package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class IncreaseState extends EntityEvent{
	public IncreaseState(Entity entity, Game game) {
		super("IncreaseState" , entity, game);
	}

	public void execute() {
		game.setEntityState(entity.getId(), game.getEntityState(entity.getId())+1);
	}
    
}
