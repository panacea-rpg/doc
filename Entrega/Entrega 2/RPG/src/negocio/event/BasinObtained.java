package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class BasinObtained extends EntityEvent {
	public BasinObtained(Entity entity, Game game) {
		super("BasinObtained", entity, game);
	}
	
   public void execute() {
	   game.setEntityState(entity.getId(), 3);
	   game.setEntityState("Fountain", 2);
	   game.addToInventory("Basin");
   }
}
