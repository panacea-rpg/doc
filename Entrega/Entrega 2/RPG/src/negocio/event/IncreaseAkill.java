package negocio.event;

import negocio.logic.Game;
import negocio.logic.Item;

public class IncreaseAkill extends ItemEvent {

	public IncreaseAkill(Item entity, Game game) {
		super("IncreaseAkill", entity, game);
	}

	@Override
	public void execute() {
		game.setEntityState("Akil", 3); 
	}

}
