package negocio.event;

import negocio.logic.Game;
import negocio.logic.Item;

public class IncreaseCaptain extends ItemEvent {

	public IncreaseCaptain(Item item,Game game) {
		super("IncreaseCaptain", item, game);
	}

	@Override
	public void execute() {
		game.setEntityState("Captain", 2);
	}

}
