package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public abstract class EntityEvent extends Event {
	protected final Entity entity;
      
	public EntityEvent(String id, Entity entity, Game game) {
		super(id, game);
		this.entity = entity;
	}

}
