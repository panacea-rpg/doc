package negocio.event;

import negocio.logic.Game;

public class Transition extends CellEvent {
	private final String destLevel;
	private final int destX;
	private final int destY;

	public Transition(int x, int y, Game game, String destLevel, int destX, int destY) {
		super("Transition", x, y, game);
		this.destLevel = destLevel;
		this.destX = destX;
		this.destY = destY;
	}

	public void execute() {
		if (game.isPlayer(x, y)) {
			game.changeLevel(destLevel, destX, destY);
			game.triggerEvent(destX, destY);
		}
	}
}

