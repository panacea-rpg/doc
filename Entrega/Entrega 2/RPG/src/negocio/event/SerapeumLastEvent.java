package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class SerapeumLastEvent extends EntityEvent {
	public SerapeumLastEvent(Entity entity, Game game) {
		super("SerapeumLastEvent", entity, game);
	}

	@Override
	public void execute() {
		game.setEntityState("Vecina", 1);
		game.setEntityState(entity.getId(), game.getEntityState(entity.getId())+1);
	}

}
