package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Facing;
import negocio.logic.Game;

public class GiveMirror extends EntityEvent {

	public GiveMirror(Entity entity, Game game) {
		super("GiveMirror", entity, game);
	}

	@Override
	public void execute() {
		game.addToInventory("mirror");
		game.setEntityState("Akil", 3);

		game.getPlayer().setFacing(Facing.RIGHT);
		game.changeLevel("UpstairsFaro", 26, 35);

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		game.getPlayer().setAct(true);
	}

}
