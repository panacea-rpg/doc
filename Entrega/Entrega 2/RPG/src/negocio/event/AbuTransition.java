package negocio.event;

import negocio.logic.Game;

public class AbuTransition extends CellEvent {

	private final String destLevel;
	private final int destX;
	private final int destY;

	public AbuTransition(int x, int y, Game game, int destX, int destY) {
		super("Transition", x, y, game);
		this.destLevel = "AbuDownstairs";
		this.destX = destX;
		this.destY = destY;
	}

	public void execute() {
		if (game.isPlayer(x, y) && game.isItem("key0")) {
			game.changeLevel(destLevel, destX, destY);
			game.triggerEvent(destX, destY);
		}
	}

}
