package negocio.event;

import negocio.logic.Game;
import negocio.logic.Item;

public class ReadLetter extends ItemEvent {
    public ReadLetter(Item letter, Game game) {
    	super("ReadLetter", letter, game);
    }

	public void execute() {
		game.setEntityState("Captain", 2);
	}
}
