package negocio.event;

import java.util.Map;

public class LogicStatesManager {
	private Map<String, Integer> entitiesStates;

	public LogicStatesManager(Map<String, Integer> entitiesStates) {
		this.entitiesStates = entitiesStates;
	}

	public Map<String, Integer> getEntitiesStates() {
		return entitiesStates;
	}

	public int getState(String key) {
		if (!entitiesStates.containsKey(key))
			entitiesStates.put(key, 0);
		return entitiesStates.get(key);
	}

	public void setState(String key, int state) {
		entitiesStates.put(key, state);
	}
	public boolean isNewGame() {
		for(Map.Entry<String, Integer> m:entitiesStates.entrySet()) {
			if(m.getValue() != 0) return false;
		}
		return true;
	}

}
