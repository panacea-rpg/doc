package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class EndGame extends EntityEvent {

	public EndGame(Entity entity, Game game) {
		super("EndGame", entity, game);
	}

	@Override
	public void execute() {
		game.endGame();
	}

}
