package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class GiveKey extends EntityEvent {

	public GiveKey(Entity entity, Game game) {
		super("GiveKey", entity, game);
	}

	@Override
	public void execute() {
		game.addToInventory("key0");
	}

}
