package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class AkillQuest extends EntityEvent {

	public AkillQuest(Entity entity, Game game) {
		super("AkillQuest", entity, game);
	}

	@Override
	public void execute() {
		game.setEntityState("Akil", 1);
	}

}
