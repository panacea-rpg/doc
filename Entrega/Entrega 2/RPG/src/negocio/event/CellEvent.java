package negocio.event;

import negocio.logic.Game;

public abstract class CellEvent extends Event{
	protected final int x;
	protected final int y;
	
	public CellEvent(String id, int x, int y, Game game) {
		super(id, game);
		this.x = x;
		this.y = y;
	}
	
}
