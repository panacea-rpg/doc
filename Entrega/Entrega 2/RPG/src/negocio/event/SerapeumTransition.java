package negocio.event;

import negocio.logic.Game;

public class SerapeumTransition extends Transition {


	public SerapeumTransition(int x, int y, Game game, String destLevel, int destX, int destY) {
		super(x, y, game, destLevel, destX, destY);
	}

	@Override
	public void execute() {
		super.execute();
		game.setCrazyMode(false);
	}

}
