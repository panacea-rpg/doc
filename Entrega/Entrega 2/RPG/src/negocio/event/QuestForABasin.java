package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class QuestForABasin extends EntityEvent {
     public QuestForABasin(Entity entity, Game game) {
    	 super("QuestForABasin", entity, game);
     }
     
     public void execute() {
    	 game.setEntityState("Fountain", 1);
    	 game.setEntityState("Adio", 2);
    	 game.setEntityState(entity.getId(), 1);
     }
}
