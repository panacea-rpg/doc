package negocio.event;

import negocio.entities.Entity;
import negocio.entities.Player;
import negocio.logic.Facing;
import negocio.logic.Game;

public class WarpPort extends EntityEvent {
	public WarpPort(Entity entity, Game game) {
		super("WarpPort" , entity, game);
		
	}

	public void execute() {
		game.setCrazyMode(true);
		Player p = game.getPlayer();
		game.changeLevel("Level0", p.getX(), p.getY());
		p.setFacing(Facing.RIGHT);
		game.changeLevel("port", 52, 52);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		game.getPlayer().setAct(true);
	}
    
}
