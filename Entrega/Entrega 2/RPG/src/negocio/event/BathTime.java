package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class BathTime extends EntityEvent {
    public BathTime(Entity entity, Game game) {
    	super("BathTime", entity , game);
    }
    
    public void execute() {
    	game.setEntityState("Darwishi", 2);
    	game.setEntityState("Fountain", 0);
    }
}
