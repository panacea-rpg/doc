package negocio.event;

import negocio.action.Act;
import negocio.action.Advance;
import negocio.logic.Facing;
import negocio.logic.Game;

public class BlockFaro extends CellEvent {
	public BlockFaro(int x, int y, Game game) {
		super("BlockFaro", x, y, game);
	}

	public void execute() {
		if (game.getEntityState("Akil") == 0) {
			game.getPlayer().addOrder(new Act(game.getPlayer()));
			game.getPlayer().addOrder(new Advance(game.getPlayer(), Facing.RIGHT));
		}else {
			game.setEntityCell(x-1,y,null);
		}
	}

}
