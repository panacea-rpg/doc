package negocio.GameMenus;

import negocio.logic.Game;
import negocio.transfer.ItemTransfer;

public abstract class Menu {
    public Game game;
	
    public Menu(Game game){
    	this.game = game;
    }
    public String getDialogMenuTransfer() {
    	return null;
    }
    public ItemTransfer getInventoryItems() {
  	 return null;
    }
	public abstract void takeOrders();
    public abstract void refresh();
}
