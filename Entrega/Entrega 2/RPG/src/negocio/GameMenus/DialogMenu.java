package negocio.GameMenus;

import negocio.entities.Entity;
import negocio.event.Event;
import negocio.logic.Dialog;
import negocio.logic.Game;
import negocio.logic.LogicInput;

public class DialogMenu extends Menu {
	private Entity interloc;
	private Dialog dialog;
	private int selectedOption;
	
    public DialogMenu(Entity interloc, Dialog dialog, Game game){
    	super(game);
    	this.interloc = interloc;
    	this.dialog = dialog;
    	this.selectedOption = 0;
    }

    
    public void takeOrders(){ 
    	if(LogicInput.ACTION.isDown()) {
    		if(selectedOption == dialog.getConversation().size() - 1) {
    			game.setMenu(null);
    			if(interloc != null)interloc.setTalking(false);
    			if(interloc != null)if(interloc.getPrevFacing()!= null)interloc.setFacing(interloc.getPrevFacing());
    			game.onDialogDestroyed();
    			if(interloc != null)if(interloc.getEvent() != null) interloc.getEvent().execute();
    		}else {
    			selectedOption++;
    			try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		LogicInput.Menulock();
    	}
    	else if (LogicInput.BACK.isDown() || LogicInput.EXIT.isDown()) {
    		game.setMenu(null);
    		if(interloc != null)interloc.setTalking(false);
    		game.onDialogDestroyed();
    		LogicInput.Menulock();
    	}
    	
    }
    public boolean takeInventoryOrders(Event event) {
    	if(LogicInput.ACTION.isDown()) {
    		if(selectedOption == dialog.getConversation().size() - 1) {
    			LogicInput.Menulock();
    			if(event!=null)event.execute();
    			return true;
    		}else {
    			selectedOption++;
    			try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		LogicInput.Menulock();
    	}
    	else if (LogicInput.BACK.isDown() || LogicInput.EXIT.isDown()||LogicInput.INVENTORY.isDown()) {
    		LogicInput.Menulock();
    		return true;
    	}
    	return false;
    }
    public String getDialogMenuTransfer() {
    	return dialog.getConversation().get(this.selectedOption);
    }
    public void refresh() {
    	
    }
}
