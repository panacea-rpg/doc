{"events":[

  {"name": "Transition",
   "level": "Faro",
   "destLevel": "Island",
   "destX": 14,
   "destY": 62,
   "x": 114,
   "y": 12
  },
  {"name": "Transition",
   "level": "Faro",
   "destLevel": "Island",
   "destX": 14,
   "destY": 63,
   "x": 114,
   "y": 13
  },
  {"name": "Transition",
   "level": "Faro",
   "destLevel": "Island",
   "destX": 14,
   "destY": 64,
   "x": 114,
   "y": 14
  },
  {"name": "Transition",
   "level": "Faro",
   "destLevel": "UpstairsFaro",
   "destX": 37,
   "destY": 34,
   "x": 19,
   "y": 12
  },
  {"name": "Transition",
   "level": "Faro",
   "destLevel": "UpstairsFaro",
   "destX": 37,
   "destY": 35,
   "x": 19,
   "y": 13
  },
  {"name": "Transition",
   "level": "Faro",
   "destLevel": "UpstairsFaro",
   "destX": 37,
   "destY": 35,
   "x": 19,
   "y": 14
  },

{"name": "BlockFaro",
  "level": Faro,
  "x": 21,
  "y": 12
},

{"name": "BlockFaro",
  "level": Faro,
  "x": 21,
  "y": 13
},

{"name": "BlockFaro",
  "level": Faro,
  "x": 21,
  "y": 14
},
]
}